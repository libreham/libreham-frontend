<?php

/**
 * This is the LibreHam newuser page.
 * Users will will create new accounts here.
 * php version 8.2
 * 
 * @category   LibreHam
 * @package    LibreHam-Frontend
 * @subpackage Newuser
 * @author     Matthew Chambers <mchambers@mchambersradio.com>
 * @copyright  2024 LibreHam Project
 * @license    https://opensource.org/license/mpl-2-0 Mozilla Public License 2.0
 * @link       https://gitlab.com/libreham/libreham-frontend
 * @since      0.0.2
 */

require 'session/database.class.php';
require 'session/mysql.sessions.php';


use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;

$session = new Session();

if (mysqli_connect_error() === true) {
    \Sentry\captureMessage(mysqli_connect_error());
    die();
}

if ($_SERVER['REQUEST_METHOD'] === 'POST') {
    // Check if all fields are set and not empty, and email is valid
    if (!empty($_POST['dispname']) 
        && !empty($_POST['email']) 
        && !empty($_POST['username']) 
        && !empty($_POST['password']) 
        && filter_var($_POST['email'], FILTER_VALIDATE_EMAIL)
    ) {
        $verificationCode = bin2hex(random_bytes(16)); 
        $userDisable = '0'; // Indicates that the user's email has not been verified

        // Proceed with inserting the new user into the database
        $myuuid = uniqid();
        $mydispname = $_POST['dispname'];
        $myemail = $_POST['email'];
        $myusername = $_POST['username'];
        $mypassword = password_hash($_POST['password'], PASSWORD_DEFAULT);

        $sql = $connHamdb->prepare(
            'INSERT INTO users (username, password, email, display_name, unique_id, 
            user_disable, verification_code) 
            VALUES (?, ?, ?, ?, ?, ?, ?);'
        );
        $sql->bind_param(
            'sssssss', 
            $myusername, $mypassword, $myemail, $mydispname, 
            $myuuid, $userDisable, $verificationCode
        );

        if ($sql->execute() === true) {
            // Initialize PHPMailer
            $mail = new PHPMailer(true);
            try {
                // Server settings
                $mail->isSMTP();
                $mail->Host = 'smtp-relay.gmail.com';
                $mail->SMTPAuth = false;
                $mail->Port = 587;
                $mail->SMTPSecure = PHPMailer::ENCRYPTION_STARTTLS;

                // Recipients
                $mail->setFrom('noreply@libreham.org', 'LibreHam');
                $mail->addAddress($myemail); // Add a recipient

                // Content
                $mail->isHTML(false); // Set email format to plain text
                $mail->Subject = 'LibreHam.org - Verify your email address';
                $mail->Body    = $mydispname . 
                    "\n\nPlease click the link below to verify your email address:
                    \n\nhttps://libreham.org/verifyemail.php?code=" . 
                    $verificationCode;

                $mail->send();
                echo "A verification email has been sent.\n\n
                    Please check your email to verify your account.";
            } catch (Exception $e) {
                echo "Verification email could not be sent.\n\n
                    Mailer Error: {$mail->ErrorInfo}";
            }
        } else {
            \Sentry\captureException($connHamdb->error);
            echo "There was an error processing your request.";
        }
    } else {
        echo "Please fill in all fields with valid information.";
    }
}


$connHamdb->close();
?>
<html>

<head>
    <title>New User Page</title>
    <link rel="stylesheet" href="style/default.css">
    </style>
</head>

<body>
    <div class="center">
        <div class="userForm">
            <div class="login-header"><b>New User</b></div>
            <div class="login-box">
                <form action="newuser.php" method="post">
                    <label>Display Name :</label>
                    <input type="text" name="dispname" class="box" required />
                    <br /><br />
                    <label>Email Address :</label>
                    <input type="text" name="email" class="box" required />
                    <br /><br />
                    <label>UserName :</label>
                    <input type="text" name="username" class="box" required />
                    <br /><br />
                    <label>Password :</label>
                    <input type="password" name="password" class="box" required />
                    <br /><br />
                    <input type="submit" value=" Submit " /><br />
                </form>

            </div>
        </div>
    </div>
</body>

</html>