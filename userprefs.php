<?php
/**
 * This is the LibreHam user preferences page.
 * Users will update their inside contact info, 
 * password, display name and api-key here.
 * php version 8.2
 * 
 * @category   LibreHam
 * @package    LibreHam-Frontend
 * @subpackage Userprefs
 * @author     Matthew Chambers <mchambers@mchambersradio.com>
 * @copyright  2024 LibreHam Project
 * @license    https://opensource.org/license/mpl-2-0 Mozilla Public License 2.0
 * @link       https://gitlab.com/libreham/libreham-frontend
 * @since      0.0.2
 */

require 'session/session.php';
?>
<html">
    <head>
        <title>Welcome to LibreHam.org</title>
        <link rel="stylesheet" href="style/default.css">
    </head>
    <body>
        <h1>Welcome <?php echo $userDispName; ?></h1>
        <a href = "index.php">Home</a> |
        <a href = "mycalls.php">Manage Callsigns</a> |
        <a href = "logout.php">Sign Out</a><br /><br />
<?php
if (mysqli_connect_error() === true) {
    \Sentry\captureMessage(mysqli_connect_error());
}

    $sqlHamdb = $connHamdb->prepare(
        'SELECT * FROM users WHERE username=? AND unique_id=?;'
    );
    $sqlHamdb->bind_param('ss', $userName, $userUuid);
    $sqlHamdb->execute();
    $result = $sqlHamdb->get_result();
    if (mysqli_num_rows($result) === 1) {
        $row = $result->fetch_assoc();
        if ($row['unique_id'] === $userUuid && $row['username'] === $userName) {
            $userEmail   = $row['email'];
            $displayname = $row['display_name'];
            $apiKey      = $row['api_key'];
            $userDisable = $row['user_disable'];
        }
    }

