<?php
    require_once 'vendor/autoload.php';

    $dotenv = Dotenv\Dotenv::createImmutable((__DIR__)); 
    $dotenv->load();

    \Sentry\init(
        [
            'dsn'         => 'https://glet_147280d1647e85e83a3bee1cbfc6d242@observe.gitlab.com:443/errortracking/api/v1/projects/47402344',
            'environment' => 'production',
            'sample_rate' => 1.0,
        ]
    );

    // Define database configuration.
    // For sessions db
    define('DB_HOST',$_ENV["DB_SERVER"]);
    define('DB_USER', $_env['DB_USER']);
    define('DB_PASS', $_ENV['DB_PASS']);
    define('DB_NAME', 'sessions');

    $connHamdb = new mysqli(DB_HOST, DB_USER, DB_PASS, $_ENV['DB_HAMDB_NAME']);
    $connUls   = new mysqli(DB_HOST, DB_USER, DB_PASS, $_ENV['DB_ULSDATA_NAME']);
