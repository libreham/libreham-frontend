<?php
/**
 * This is the LibreHam add callsign function.
 * This function is called by create callsign to do the actual database insert.
 * php version 8.2
 * 
 * @category   LibreHam
 * @package    LibreHam-Frontend
 * @subpackage Addcallsign
 * @author     Matthew Chambers <mchambers@mchambersradio.com>
 * @copyright  2024 LibreHam Project
 * @license    https://opensource.org/license/mpl-2-0 Mozilla Public License 2.0
 * @link       https://gitlab.com/libreham/libreham-frontend
 * @since      0.0.2
 */

require 'session/session.php';

/**
 * Function to prevent SQL injection
 * 
 * @param mixed $data Input to be santized
 * 
 * @return string Santized output
 */
function sanitizeInput($data)
{
    global $connHamdb; 
    return mysqli_real_escape_string($connHamdb, trim($data));
}

/**
 * Function to convert checkbox "on" to 'Y"
 * 
 * @param mixed $option Checkbox status
 * 
 * @return string either Y or ''
 */
function checkHideOptions($option)
{
    return isset($_POST[$option]) && $_POST[$option] === 'on' ? 'Y' : '';
}

if ($_SERVER['REQUEST_METHOD'] === 'POST') {
    $fields = [
        'uuid', 'callsign', 'first_name', 'mi', 'last_name', 'suffix',
        'nickname', 'disp_email', 'qsl_manager', 'po_box', 'street_address',
        'city', 'county', 'state', 'zip_code', 'gridsquare',
    ];

    $data = [];
    foreach ($fields as $field) {
        $data[$field] = sanitizeInput($_POST[$field] ?? '');
    }

    $hideOptions = [
        'hide_pobox', 'hide_address', 'hide_city', 'hide_county',
        'hide_state', 'hide_postal', 'hide_gridsquare',
    ];

    foreach ($hideOptions as $option) {
        $data[$option] = checkHideOptions($option);
    }

    // Check for ULS
    $data['is_uls'] = isset($_POST['is_uls']) && $_POST['is_uls'] === 'Y' ? 'Y' : '';

    // Proceed with ULS specific logic
    if ($data['is_uls'] === 'Y') {
        // ...ULS logic here...
    }

    // SQL insert statement
    $stmt = $connHamdb->prepare(
        'INSERT INTO callsigns (
            unique_id, callsign, is_uls, first_name, mi, last_name, suffix,
            nick_name, disp_email, qsl_manager, po_box, street_address, city,
            county, state, gridsquare, postal_code, hide_pobox, hide_address, 
            hide_city, hide_county, hide_state, hide_postal, hide_gridsquare
        ) VALUES (
            ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, 
            ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?);'
    );

    if (!$stmt) {
        \Sentry\captureMessage("Prepare failed: " . $connHamdb->error);
        exit;
    }

    $stmt->bind_param(
        'ssssssssssssssssssssssss', 
        $data['uuid'], $data['callsign'], $data['is_uls'], $data['first_name'], 
        $data['mi'], $data['last_name'], $data['suffix'], $data['nickname'], 
        $data['disp_email'], $data['qsl_manager'], $data['po_box'],
        $data['street_address'], $data['city'], $data['county'], $data['state'],
        $data['gridsquare'], $data['zip_code'], $data['hide_pobox'], 
        $data['hide_address'], $data['hide_city'], $data['hide_county'], 
        $data['hide_state'], $data['hide_postal'], $data['hide_gridsquare']
    );

    if ($stmt->execute()) {
        header('Location: mycalls.php');
    } else {
        \Sentry\captureMessage("Execute failed: " . $stmt->error);
    }

    $stmt->close();
    $connHamdb->close();
}

\Sentry\captureLastError();
