<?php

/**
 * This is the LibreHam verifyemail page.
 * Users will will validate email for new accounts here.
 * php version 8.2
 * 
 * @category   LibreHam
 * @package    LibreHam-Frontend
 * @subpackage Verifyemail
 * @author     Matthew Chambers <mchambers@mchambersradio.com>
 * @copyright  2024 LibreHam Project
 * @license    https://opensource.org/license/mpl-2-0 Mozilla Public License 2.0
 * @link       https://gitlab.com/libreham/libreham-frontend
 * @since      0.0.2
 */

require 'session/database.class.php';
require 'session/mysql.sessions.php';
$session = new Session();

if (mysqli_connect_error() === true) {
    \Sentry\captureMessage(mysqli_connect_error());
    die();
}
if (isset($_GET['code'])) {
    $code = $_GET['code'];

    // Assuming $connHamdb is your database connection variable
    $sql = $connHamdb->prepare(
        "UPDATE users SET user_disable = NULL, verification_code = NULL 
        WHERE verification_code = ? AND user_disable = '0'"
    );
    $sql->bind_param('s', $code);

    if ($sql->execute()) {
        // Check if any rows were updated
        if ($sql->affected_rows > 0) {
            echo "Your email has been verified successfully. 
            Your account is now active.";
            // Redirect or provide a link to the login page
        } else {
            echo "Invalid or expired verification code.";
            // Handle error or re-send verification email
        }
    } else {
        // Handle SQL error
        \Sentry\captureException($connHamdb->error);
    }
}
