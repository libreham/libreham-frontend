<?php

/**
 * This is the LibreHam check call callback function
 * This function is called from the new call page to check if a call is
 * available and pre-populate ULS data
 * php version 8.2
 * 
 * @category   LibreHam
 * @package    LibreHam-Frontend
 * @subpackage Checkcall
 * @author     Matthew Chambers <mchambers@mchambersradio.com>
 * @copyright  2024 LibreHam Project
 * @license    https://opensource.org/license/mpl-2-0 Mozilla Public License 2.0
 * @link       https://gitlab.com/libreham/libreham-frontend
 * @since      0.0.2
 */


require 'session/session.php';
$call = htmlspecialchars($_GET['call']); // Sanitize the input

// Check the database connection right after it's made
if (mysqli_connect_error()) {
    \Sentry\captureMessage(mysqli_connect_error());
    die("Database connection failed: " . mysqli_connect_error());
}

/**
 * Function to generate form fields
 * 
 * @param array $data array of data to repopulate the fields
 * 
 * @return null no return value, outputs directly to UI
 */
function outputFormFields($data = [])
{
    // Dynamically outputs the form fields
    $fields = [
        'first_name' => 'First Name',
        'mi' => 'Middle Initial',
        'last_name' => 'Last Name',
        'suffix' => 'Suffix',
        'nickname' => 'Nickname',
        'disp_email' => 'Displayed Email',
        'qsl_manager' => 'QSL Manager',
        'po_box' => 'PO BOX',
        'street_address' => 'Street Address',
        'city' => 'City',
        'state' => 'State',
        'zip_code' => 'Zip Code',
        'county' => 'County',
        'gridsquare' => 'Gridsquare',
    ];

    foreach ($fields as $key => $label) {
        $value = $data[$key] ?? '';
        $value = htmlspecialchars($value);
        echo "<label>$label : </label>";
        if (strpos($key, 'hide_') === 0) {
            // If the field is meant to be a checkbox
            $checked = $value ? 'checked' : '';
            echo "<input type='checkbox' name='$key' $checked><br />\n";
        } else {
            // For text inputs
            echo "<input type='text' name='$key' 
                class='box' value='$value'><br />\n";
        }
    }

    // Always output these fields
    echo "<input type='hidden' name='uuid' value ='" . 
        htmlspecialchars($GLOBALS['userUuid']) . "'>\n";
    echo "<input type='submit' value='Save' 
        onkeydown='return event.key != \"Enter\";'>\n";
}

// First, check if the callsign is already taken
$sqlHamdb = $connHamdb->prepare('SELECT callsign FROM callsigns WHERE callsign=?;');
$sqlHamdb->bind_param('s', $call);
$sqlHamdb->execute();
$result = $sqlHamdb->get_result();
if ($result && mysqli_num_rows($result) > 0) {
    echo 'Callsign ' . $call . ' is taken.';
} else {
    // If not taken, proceed to check in the ULS database
    $sqlUls = $connUls->prepare(
        'SELECT 
            PUBACC_AM.callsign, EN_ApplicantType.app_type_name, 
            AM_OperatorClass.class_name, HD_LicenseStatus.status_name, 
            PUBACC_HD.radio_service_code, PUBACC_EN.last_name, 
            PUBACC_EN.first_name, PUBACC_EN.mi, PUBACC_EN.suffix, 
            PUBACC_EN.attention_line, PUBACC_EN.po_box, PUBACC_EN.street_address, 
            PUBACC_EN.city, PUBACC_EN.state, PUBACC_EN.zip_code, 
            PUBACC_HD.grant_date 
        FROM PUBACC_AM 
        INNER JOIN PUBACC_EN
        ON PUBACC_AM.unique_system_identifier=PUBACC_EN.unique_system_identifier 
        INNER JOIN PUBACC_HD 
        ON PUBACC_AM.unique_system_identifier=PUBACC_HD.unique_system_identifier 
        INNER JOIN EN_ApplicantType 
        ON PUBACC_EN.applicant_type_code=EN_ApplicantType.applicant_type_code 
        INNER JOIN AM_OperatorClass 
        ON PUBACC_AM.operator_class=AM_OperatorClass.operator_class 
        INNER JOIN HD_LicenseStatus 
        ON PUBACC_HD.license_status=HD_LicenseStatus.license_status 
        WHERE PUBACC_AM.callsign = ? 
        ORDER BY PUBACC_HD.license_status 
        ASC LIMIT 1;'
    );
    $sqlUls->bind_param('s', $call);
    $sqlUls->execute();
    $resultUls = $sqlUls->get_result();
    if ($resultUls && mysqli_num_rows($resultUls) > 0) {
        $row = $resultUls->fetch_assoc();
        echo "Your callsign found in the FCC's ULS database...";
        outputFormFields($row); // Call with data to pre-populate fields
    } else {
        echo "Enter your details";
        outputFormFields(); // Call without parameters for empty fields
    }
}

$connHamdb->close();
$connUls->close();
\Sentry\captureLastError();
