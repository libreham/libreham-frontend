<?php
/**
 * This is the LibreHam session function
 * TShis function checks if the current session is valid, if not it calls the logout function.
 *
 * @package    LibreHam
 * @subpackage session
 * @author     Matthew Chambers <mchambers@mchambersradio.com>
 * @copyright  2023 LibreHam Project
 * @since      0.0.1
 */

    require 'database.class.php';
    require 'mysql.sessions.php';
    $session = new Session();

if (isset($_SESSION['user']) === false) {
    header('location:index.php');
    die();
}

    $userName = $_SESSION['user'];
    $userUuid = $_SESSION['uuid'];
    $sql      = $connHamdb->prepare('SELECT display_name, user_disable, admin_disable FROM users WHERE username=? and unique_id=?;');
    $sql->bind_param('ss', $userName, $userUuid);
    $sql->execute();
    $result = $sql->get_result();
    $row    = $result->fetch_assoc();
if ($row['admin_disable'] === 'D' || $row['user_disable'] === '0') {
    header('location: logout.php');
    die();
}

    $userDispName = $row['display_name'];
    \Sentry\captureLastError();
