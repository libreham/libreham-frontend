<?php
/**
 * This is the LibreHam user login page.
 * Users will login to the site here.
 * php version 8.2
 * 
 * @category   LibreHam
 * @package    LibreHam-Frontend
 * @subpackage Login
 * @author     Matthew Chambers <mchambers@mchambersradio.com>
 * @copyright  2024 LibreHam Project
 * @license    https://opensource.org/license/mpl-2-0 Mozilla Public License 2.0
 * @link       https://gitlab.com/libreham/libreham-frontend
 * @since      0.0.2
 */

    require 'session/database.class.php';
    require 'session/mysql.sessions.php';
    $session = new Session();
    $error   = '';
if (mysqli_connect_error() === true) {
    \Sentry\captureMessage(mysqli_connect_error());
}

if ($_SERVER['REQUEST_METHOD'] === 'POST') {
    // Username and password.
    $myusername = $_POST['username'];
    $mypassword = $_POST['password'];
    $sql        = $connHamdb->prepare('SELECT * FROM users WHERE username = ?;');
    $sql->bind_param('s', $myusername);
    $sql->execute();
    $result = $sql->get_result();
    $row    = $result->fetch_assoc();
    if (mysqli_num_rows($result) === 1) {
        if (password_verify($mypassword, $row['password']) === true) {
            $_SESSION['user'] = $myusername;
            $_SESSION['uuid'] = $row['unique_id'];
            if ($row['admin_disable'] === 'D') {
                $error = 'Account administratively disabled, contact support.';
            } else if ($row['user_disable'] === '0') {
                $error = 'Account email not verified, verify email address.';
            } else {                
                header('location: index.php');
                die();
            }
        } else {
            $error = 'Your username and/or password are invalid';
        }
    } else {
        \Sentry\captureLastError();
    }//end if

    $connHamdb->close();
}//end if
?>
<html>
    <head>
        <title>Login Page</title>
        <link rel="stylesheet" href="style/default.css">
    </style>  
    </head>
    <body>
        <div class="center">
            <div class="content">
            <div class="login-header"><b>Login</b></div>    
            <div class="login-box">
                <form action = "" method = "post">
                    <label>UserName  :</label>
                    <input type="text" name="username" class="box"/>
                    <br /><br />
                    <label>Password  :</label>
                    <input type="password" name="password" class="box"/>
                    <br/><br />
                    <input type = "submit" value = " Submit "/><br />
                </form>
                <div class="error"><?php echo $error; ?></div>        
            </div>    
            </div>    
        </div>
    </body>
</html>
