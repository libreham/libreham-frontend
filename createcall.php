<?php
/**
 * This is the LibreHam create call detail page
 * This is create callsign detail page.
 * php version 8.2
 * 
 * @category   LibreHam
 * @package    LibreHam-Frontend
 * @subpackage Createcall
 * @author     Matthew Chambers <mchambers@mchambersradio.com>
 * @copyright  2024 LibreHam Project
 * @license    https://opensource.org/license/mpl-2-0 Mozilla Public License 2.0
 * @link       https://gitlab.com/libreham/libreham-frontend
 * @since      0.0.2
 */


require 'session/session.php';
?>
<html">
    <head>
        <title>Welcome to LibreHam.org</title>
        <link rel="stylesheet" href="style/default.css">
        <script>
        $(document).ready(function() 
        {
            $(window).keydown(function(event)
            {
                if(event.keyCode == 13) 
                {
                    event.preventDefault();
                    return false;
                }
            });
        });
        function checkCall(str)
        {
            if (str == "") 
            {
                document.getElementById("newCallForm").innerHTML = "";
                return;
            } else 
            {
                var xmlhttp = new XMLHttpRequest();
                xmlhttp.onreadystatechange = function() 
                {
                    if (this.readyState == 4 && this.status == 200) 
                    {
                        document.getElementById("newCallForm").innerHTML = 
                            this.responseText;
                    }
                };
                xmlhttp.open("GET","checkcall.php?call="+str,true);
                xmlhttp.send();
            }
        }
        </script>
    </head>
    <body>
        <h1>Welcome <?php echo $userDispName; ?></h1>
        <a href = "index.php">Home</a> |
        <a href = "userprefs.php">User Preferences</a> |
        <a href = "logout.php">Sign Out</a><br /><br />

        <form action="addcallsign.php" method="post" 
            onkeydown="return event.key != 'Enter';">
            <label>Callsign : </label>
            <input type="text" name="callsign" class="box" 
                onchange="checkCall(this.value)"><br />
            <div id="newCallForm"></div>
        </form>
    </body>
</html>