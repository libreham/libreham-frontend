<?php
/**
 * This is the LibreHam result display page.
 * This outputs the results of a search.
 * php version 8.2
 * 
 * @category   LibreHam
 * @package    LibreHam-Frontend
 * @subpackage Search_Result_Display
 * @author     Matthew Chambers <mchambers@mchambersradio.com>
 * @copyright  2024 LibreHam Project
 * @license    https://opensource.org/license/mpl-2-0 Mozilla Public License 2.0
 * @link       https://gitlab.com/libreham/libreham-frontend
 * @since      0.0.2
 */

// Ensure there's a search result to display
if (!isset($searchResult) || empty($searchResult)) {
    echo "<p>No results found.</p>";
    return;
}

if ($type === 'call') {
    // Display for a single callsign search result
    if (empty($searchResult[0]['callsign'])) {
        echo "<p>Callsign not found or no information available.</p>";
        return;
    }

    // Basic information always shown
    echo "<h1>" . htmlspecialchars($searchResult[0]['callsign']) . "</h1>\n";
    echo "<h3>" . htmlspecialchars($searchResult[0]['first_name']) . " " . 
        htmlspecialchars($searchResult[0]['mi']);
    if (!empty($searchResult[0]['nick_name'])) {
        echo ' "' . htmlspecialchars($searchResult[0]['nick_name']) . '"';
    }
    echo " " . htmlspecialchars($searchResult[0]['last_name']);
    if (!empty($searchResult[0]['suffix'])) {
        echo " " . htmlspecialchars($searchResult[0]['suffix']);
    }
    echo "</h3>\n";

    // Detailed information for logged-in users
    if ($sessionValid) {
        if (!empty($searchResult[0]['qsl_manager'])) {
            echo "<p>QSL via: " . 
            htmlspecialchars($searchResult[0]['qsl_manager']) . "</p>\n";
        }

        // Address information
        if (!empty($searchResult[0]['attention_line'])) {
            echo "<p>Attention: " . 
            htmlspecialchars($searchResult[0]['attention_line']) . "</p>\n";
        }
        if (!empty($searchResult[0]['po_box'])) {
            echo "<p>PO BOX: " . 
            htmlspecialchars($searchResult[0]['po_box']) . "</p>\n";
        }
        if (!empty($searchResult[0]['street_address'])) {
            echo "<p>Address: " . 
            htmlspecialchars($searchResult[0]['street_address']) . "</p>\n";
        }
        if (!empty($searchResult[0]['city'])) {
            echo "<p>City: " . 
            htmlspecialchars($searchResult[0]['city']);
        }
        if (!empty($searchResult[0]['state'])) {
            echo ", " . 
            htmlspecialchars($searchResult[0]['state']) . "</p>\n";
        }
        if (!empty($searchResult[0]['postal_code'])) {
            echo "<p>Postal Code: " . 
            htmlspecialchars($searchResult[0]['postal_code']) . "</p>\n";
        }
        if (!empty($searchResult[0]['country'])) {
            echo "<p>Country: " . 
            htmlspecialchars($searchResult[0]['country']) . "</p>\n";
        }
        if (!empty($searchResult[0]['disp_email'])) {
            echo "<p>Email: " . 
            htmlspecialchars($searchResult[0]['disp_email']) . "</p>\n";
        }
        if (!empty($searchResult[0]['county'])) {
            echo "<p>County: " . 
            htmlspecialchars($searchResult[0]['county']) . " County</p>\n";
        }
        if (!empty($searchResult[0]['gridsquare'])) {
            echo "<p>Gridsquare: " . 
            htmlspecialchars($searchResult[0]['gridsquare']) . "</p>\n";
        }
    } else {
        echo "<p>Sign in to see full information.</p>";
    }
} elseif ($type === 'city' || $type === 'name') {
    // Display for searches by name or city with multiple results
    echo "<table>\n<tr><th>Callsign</th><th>Name</th>
        <th>City</th><th>State</th><th>Country</th></tr>\n";

    foreach ($searchResult as $callResult) {
        echo "<tr>\n";
        echo "<td><a href=\"javascript:void(0);\" onclick=\"submitSearch('" . 
            htmlspecialchars($callResult['callsign']) . "');\">" . 
            htmlspecialchars($callResult['callsign']) . "</a></td>";
        echo "<td>" . htmlspecialchars($callResult['first_name']) . 
            " " . htmlspecialchars($callResult['mi']);
        if (!empty($callResult['nick_name'])) {
            echo ' "' . htmlspecialchars($callResult['nick_name']) . '"';
        }
        echo " " . htmlspecialchars($callResult['last_name']) . "</td>";
        echo "<td>" . htmlspecialchars($callResult['city'] ?? '&nbsp;') . "</td>";
        echo "<td>" . htmlspecialchars($callResult['state'] ?? '&nbsp;') . "</td>";
        echo "<td>" . htmlspecialchars($callResult['country'] ?? '&nbsp;') . "</td>";
        echo "</tr>\n";
    }

    echo "</table>\n";
}
