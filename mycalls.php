<?php
/**
 * This is the LibreHam my calls function.
 * This retrieves an array of all callsigns for the current user and outputs
 * to the edit callsign detail list.
 * php version 8.2
 * 
 * @category   LibreHam
 * @package    LibreHam-Frontend
 * @subpackage Mycalls
 * @author     Matthew Chambers <mchambers@mchambersradio.com>
 * @copyright  2024 LibreHam Project
 * @license    https://opensource.org/license/mpl-2-0 Mozilla Public License 2.0
 * @link       https://gitlab.com/libreham/libreham-frontend
 * @since      0.0.2
 */

require 'session/session.php';
?>
<html>
    <head>
        <title>Welcome to LibreHam.org</title>
        <link rel="stylesheet" href="style/default.css">
    </head>
    <body>
        <h1>Welcome <?php echo $userDispName; ?></h1>
        <a href = "index.php">Home</a> |
        <a href = "userprefs.php">User Preferences</a> |
        <a href = "logout.php">Sign Out</a><br /><br />

<?php
if (mysqli_connect_error() === true) {
    \Sentry\captureMessage(mysqli_connect_error());
}

    $sql = $connHamdb->prepare('SELECT callsign FROM callsigns WHERE unique_id=? ;');
    $sql->bind_param('s', $userUuid);
    $sql->execute();
    $result = $sql->get_result();
if (mysqli_num_rows($result) > 0) {
    foreach ($result as $row) {
        echo "<a href='editcall.php?call=".$row['callsign']."'>".
        $row['callsign'].'</a><br />';
    }
} else {
    echo 'No callsigns found for user.';
}

    $connHamdb->close();
    \Sentry\captureLastError();
?>
        <a href = "createcall.php">Create New Callsign</a>

    </body>
</html>
