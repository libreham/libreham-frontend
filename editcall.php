<?php
/**
 * This is the LibreHam edit call detail page
 * This is edit callsign detail page.
 * php version 8.2
 * 
 * @category   LibreHam
 * @package    LibreHam-Frontend
 * @subpackage Editcall
 * @author     Matthew Chambers <mchambers@mchambersradio.com>
 * @copyright  2024 LibreHam Project
 * @license    https://opensource.org/license/mpl-2-0 Mozilla Public License 2.0
 * @link       https://gitlab.com/libreham/libreham-frontend
 * @since      0.0.2
 */

    require 'session/session.php';

    $call = '';
    $data = [
        'callsign'       => '',
        'app_type'       => '',
        'operator_class' => '',
        'license_status' => '',
        'radio_service'  => '',
        'last_name'      => '',
        'first_name'     => '',
        'mi'             => '',
        'nick_name'      => '',
        'suffix'         => '',
        'disp_email'     => '',
        'qsl_manager'    => '',
        'attention_line' => '',
        'po_box'         => '',
        'street_address' => '',
        'city'           => '',
        'county'         => '',
        'state'          => '',
        'postal_code'    => '',
        'country'        => '',
        'gridsquare'     => '',
        'grant_date'     => '',
    ];

    if ($_SERVER['REQUEST_METHOD'] === 'POST') {
        $isUls       = '';
        $hidePoBox   = '';
        $hideAddress = '';
        $hideCity    = '';
        $hideCounty  = '';
        $hideState   = '';
        $hidePostal  = '';
        $accountUuid = $_POST['uuid'];
        $callsign    = $_POST['callsign'];
        $firstName   = $_POST['first_name'];
        $mi          = $_POST['mi'];
        $lastName    = $_POST['last_name'];
        $suffix      = $_POST['suffix'];
        $nickname    = $_POST['nickname'];
        $dispEmal    = $_POST['disp_email'];
        $qslManager  = $_POST['qsl_manager'];
        if (isset($_POST['hide_pobox']) === true
            && $_POST['hide_pobox'] === 'on'
        ) {
            $hidePoBox = 'Y';
        }

        if (isset($_POST['hide_address']) === true 
            && $_POST['hide_address'] === 'on'
        ) {
            $hideAddress = 'Y';
        }

        if (isset($_POST['hide_city']) === true 
            && $_POST['hide_city'] === 'on'
        ) {
            $hideCity = 'Y';
        }

        if (isset($_POST['hide_county']) === true 
            && $_POST['hide_county'] === 'on'
        ) {
            $hideCounty = 'Y';
        }

        if (isset($_POST['hide_state']) === true 
            && $_POST['hide_state'] === 'on'
        ) {
            $hideState = 'Y';
        }

        if (isset($_POST['hide_postal']) === true 
            && $_POST['hide_postal'] === 'on'
        ) {
            $hidePostal = 'Y';
        }

        if (isset($_POST['hide_gridsquare']) === true 
            && $_POST['hide_gridsquare'] === 'on'
        ) {
            $hideGridsquare = 'Y';
        }

        $poBox         = $_POST['po_box'];
        $streetAddress = $_POST['street_address'];
        $city          = $_POST['city'];
        $county        = $_POST['county'];
        $state         = $_POST['state'];
        $postalCode    = $_POST['zip_code'];
        $gridsquare    = $_POST['gridsquare'];
        if (isset($_POST['is_uls']) === true && $_POST['is_uls'] === 'Y') {
            $isUls = 'Y';
            if (mysqli_connect_error() === true) {
                \Sentry\captureMessage(mysqli_connect_error());
                die();
            }

            $sqlUls = $connUls->prepare(
                'SELECT
                    PUBACC_AM.callsign, 
                    EN_ApplicantType.app_type_name, 
                    AM_OperatorClass.class_name, 
                    HD_LicenseStatus.status_name, 
                    PUBACC_HD.radio_service_code, 
                    PUBACC_EN.last_name, 
                    PUBACC_EN.first_name, 
                    PUBACC_EN.mi, 
                    PUBACC_EN.suffix, 
                    PUBACC_EN.attention_line, 
                    PUBACC_EN.po_box, 
                    PUBACC_EN.street_address, 
                    PUBACC_EN.city, 
                    PUBACC_EN.state, 
                    PUBACC_EN.zip_code, 
                    PUBACC_HD.grant_date 
                FROM 
                    PUBACC_AM 
                INNER JOIN PUBACC_EN 
                    ON PUBACC_AM.unique_system_identifier=PUBACC_EN.unique_system_identifier 
                INNER JOIN PUBACC_HD
                    ON PUBACC_AM.unique_system_identifier=PUBACC_HD.unique_system_identifier 
                INNER JOIN EN_ApplicantType
                    ON PUBACC_EN.applicant_type_code=EN_ApplicantType.applicant_type_code 
                INNER JOIN AM_OperatorClass 
                    ON PUBACC_AM.operator_class=AM_OperatorClass.operator_class 
                INNER JOIN HD_LicenseStatus 
                    ON PUBACC_HD.license_status=HD_LicenseStatus.license_status 
                WHERE 
                    PUBACC_AM.callsign=?
                ORDER BY 
                    PUBACC_HD.license_status ASC
                LIMIT 1;'
            );
            $sqlUls->bind_param('s', $callsign);
            $sqlUls->execute();
            $resultUls = $sqlUls->get_result();
            $row       = $resultUls->fetch_assoc();
            if (mysqli_num_rows($resultUls) > 0) {
                if ($row['first_name'] === $_POST['first_name']) {
                    $firstName = '';
                }

                if ($row['mi'] === $_POST['mi']) {
                    $mi = '';
                }

                if ($row['last_name'] === $_POST['last_name']) {
                    $lastName = '';
                }

                if ($row['suffix'] === $_POST['suffix']) {
                    $suffix = '';
                }

                if ($row['po_box'] === $_POST['po_box']) {
                    $poBox = '';
                }

                if ($row['street_address'] === $_POST['street_address']) {
                    $streetAddress = '';
                }

                if ($row['city'] === $_POST['city']) {
                    $city = '';
                }

                if ($row['state'] === $_POST['state']) {
                    $state = '';
                }

                if ($row['zip_code'] === $_POST['zip_code']) {
                    $postalCode = '';
                }
            }//end if

            $connUls->close();
        }//end if

        if (mysqli_connect_error() === true) {
            \Sentry\captureMessage(mysqli_connect_error());
            die();
        }

        $sqlHamdb = $connHamdb->prepare(
            'UPDATE
                callsigns
            SET
                is_uls = ?, 
                first_name = ?,
                mi = ?,
                last_name = ?,
                suffix = ?, 
                nick_name = ?, 
                disp_email = ?, 
                qsl_manager = ?, 
                po_box = ?, 
                street_address = ?, 
                city = ?, 
                county = ?, 
                state = ?, 
                gridsquare = ?, 
                postal_code = ?, 
                hide_pobox = ?, 
                hide_address = ?, 
                hide_city = ?, 
                hide_county = ?, 
                hide_state = ?, 
                hide_postal = ?, 
                hide_gridsquare = ? 
            WHERE 
                unique_id = ? 
                AND callsign = ?;'
        );
        $sqlHamdb->bind_param(
            'ssssssssssssssssssssssss',
            $isUls,
            $firstName,
            $mi,
            $lastName,
            $suffix,
            $nickname,
            $dispEmal,
            $qslManager,
            $poBox,
            $streetAddress,
            $city,
            $county,
            $state,
            $gridsquare,
            $postalCode,
            $hidePoBox,
            $hideAddress,
            $hideCity,
            $hideCounty,
            $hideState,
            $hidePostal,
            $hideGridsquare,
            $userUuid,
            $callsign
        );
        if ($sqlHamdb->execute() === true) {
            header('location: mycalls.php');
        } else {
            echo $connHamdb->error;
        }
    }//end if

    if ($_SERVER['REQUEST_METHOD'] === 'GET') {
        $call = $_GET['call'];
        if (mysqli_connect_error() === true) {
            \Sentry\captureMessage(mysqli_connect_error());
            die();
        }

        $sqlHamdb = $connHamdb->prepare('SELECT * FROM callsigns WHERE callsign =?;');
        $sqlHamdb->bind_param('s', $call);
        $sqlHamdb->execute();
        $result = $sqlHamdb->get_result();
        if (mysqli_num_rows($result) > 0) {
            $row = $result->fetch_assoc();
            if ($row['unique_id'] !== $userUuid) {
                die("Call doesn't belong to user!");
            } else {
                foreach ($row as $key => $value) {
                    $data[$key] = $value;
                }
            }
        }
    }//end if
    ?>
<html">
    <head>
        <title>Welcome to LibreHam.org</title>
        <link rel="stylesheet" href="style/default.css">
    </head>
        <h1>Welcome <?php echo $userDispName; ?></h1>
        <a href = "index.php">Home</a> |
        <a href = "userprefs.php">User Preferences</a> |
        <a href = "logout.php">Sign Out</a><br /><br />

        <form action="editcall.php" method="post">
        <label>Callsign : </label>
        <input type="text" name="callsign" class="box" readonly value=
            "<?php echo $call; ?>"><br />
        <label>First Name : </label>
        <input type="text" name="first_name" class="box" value=
            "<?php echo $data['first_name']; ?>"><br />
        <label>Middle Initial : </label>
        <input type="text" name="mi" class="box" value=
            "<?php echo $data['mi']; ?>"><br />
        <label>Last Name : </label>
        <input type="text" name="last_name" class="box" value=
            "<?php echo $data['last_name']; ?>"><br />
        <label>Suffix : </label>
        <input type="text" name="suffix" class="box" value=
            "<?php echo $data['suffix']; ?>"><br />
        <label>Nickname : </label>
        <input type="text" name="nickname" class="box" value=
            "<?php echo $data['nick_name']; ?>"><br /><br />
        <label>Displayed Email : </label>
        <input type="text" name="disp_email" class="box" value=
            "<?php echo $data['disp_email']; ?>"><br />
        <label>QSL Manager : </label>
        <input type="text" name="qsl_manager" class="box" value=
            "<?php echo $data['qsl_manager']; ?>"><br /><br />
        <label>Check box to hide entered information from lookups</label><br />
        <label>PO BOX (blank if none) : </label>
        <input type="checkbox" name="hide_pobox">
        <input type="text" name="po_box" class="box" value=
            "<?php echo $data['po_box']; ?>"><br />
        <label>Street Address : </label>
        <input type="checkbox" name="hide_address">
        <input type="text" name="street_address" class="box" value=
            "<?php echo $data['street_address']; ?>"><br />
        <label>City : </label>
        <input type="checkbox" name="hide_city">
        <input type="text" name="city" class="box" value=
            "<?php echo $data['city']; ?>"><br />
        <label>County: </label>
        <input type="checkbox" name="hide_county">
        <input type="text" name="county" class="box" value=
            "<?php echo $data['county']; ?>"></br />
        <label>State : </label>
        <input type="checkbox" name="hide_state">
        <input type="text" name="state" class="box" value=
            "<?php echo $data['state']; ?>"><br />
        <label>Zip Code : </label>
        <input type="checkbox" name="hide_postal">
        <input type="text" name="zip_code" class="box" value=
            "<?php echo $data['postal_code']; ?>"><br />
        <label>Gridsquare : </label>
        <input type="checkbox" name="hide_gridsquare">
        <input type="text" name="gridsquare" class="box" value=
            "<?php echo $data['gridsquare']; ?>"></br />
        <input type="hidden" name="is_uls" value=
            "<?php echo $data['is_uls']; ?>">
        <input type="hidden" name="uuid" value=
            "<?php echo $userUuid; ?>">
        <input type="submit" value="Save">
        </form>
    </body>
</html>
<?php
\Sentry\captureLastError();

