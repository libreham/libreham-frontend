<?php
/**
 * This is the LibreHam user logout function.
 * When called, this function logs the current user out 
 * and returns them to the home page.
 * php version 8.2
 * 
 * @category   LibreHam
 * @package    LibreHam-Frontend
 * @subpackage Logout
 * @author     Matthew Chambers <mchambers@mchambersradio.com>
 * @copyright  2024 LibreHam Project
 * @license    https://opensource.org/license/mpl-2-0 Mozilla Public License 2.0
 * @link       https://gitlab.com/libreham/libreham-frontend
 * @since      0.0.2
 */

    require 'session/session.php';

    session_unset();
if (session_destroy() === true) {
    header('Location: index.php');
}
