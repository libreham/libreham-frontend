<?php

/**
 * This is the LibreHam main page.
 * This is the main page that users start on and initiate searches.
 * php version 8.2
 * 
 * @category   LibreHam
 * @package    LibreHam-Frontend
 * @subpackage Index
 * @author     Matthew Chambers <mchambers@mchambersradio.com>
 * @copyright  2024 LibreHam Project
 * @license    https://opensource.org/license/mpl-2-0 Mozilla Public License 2.0
 * @link       https://gitlab.com/libreham/libreham-frontend
 * @since      0.0.2
 */
 
require 'session/database.class.php';
require 'session/mysql.sessions.php';
$session = new Session();
$sessionValid = false;
$search = '';
$type = 'call';
$jsonResponse = '';

if (isset($_SESSION['user'])) {
    $sessionValid = true;
    $userName = $_SESSION['user'];
    $userUuid = $_SESSION['uuid'];
    $sql = $connHamdb->prepare(
        'SELECT display_name FROM users WHERE username=? AND unique_id=?;'
    );
    $sql->bind_param('ss', $userName, $userUuid);
    $sql->execute();
    $result = $sql->get_result();
    $row = $result->fetch_assoc();
    $userDispName = $row['display_name'] ?? 'User';
}

if ($_SERVER['REQUEST_METHOD'] === 'POST' 
    && !empty($_POST['search']) && !empty($_POST['type'])
) {
    $search = htmlspecialchars($_POST['search']);
    $type = htmlspecialchars($_POST['type']);

    $url = 'https://api.libreham.org/v0/query.php?' . 
        http_build_query([$type => $search, 'api_key' => $_ENV['API_KEY']]);
    $jsonResponse = file_get_contents($url);
} else {
    \Sentry\captureLastError();
}
?>
<html>

<head>
    <title>Welcome to LibreHam.org</title>
    <link rel="stylesheet" href="style/default.css">
</head>

<body>
    <div class="center">
        <div class="content">
            <?php if ($sessionValid) : ?>
                <h1>Welcome <?php echo htmlspecialchars($userDispName); ?></h1>
                <a href='userprefs.php'>User Preferences</a> |
                <a href='mycalls.php'>Manage Callsigns</a> |
                <a href='logout.php'>Sign Out</a>
            <?php else : ?>
                <h1>Welcome Guest</h1>
                <a href='login.php'>Sign In</a> |
                <a href='newuser.php'>Sign Up</a>
            <?php endif; ?>
            <div>
                <form action="index.php" method="post">
                    <label>Search: </label>
                    <input type="text" name="search" 
                        value="<?php echo htmlspecialchars($search); ?>">
                    <select id="type" name="type">
                        <option value="call"
                            <?php echo $type === 'call' ? 'selected' : ''; ?>>
                            Callsign</option>
                        <option value="name"
                            <?php echo $type === 'name' ? 'selected' : ''; ?>>
                            Name</option>
                        <option value="city"
                            <?php echo $type === 'city' ? 'selected' : ''; ?>>
                            City</option>
                    </select>
                    <input type="submit" value="Search">
                </form>
            </div>
            <div id="search_result">
                <?php if (!empty($jsonResponse)) : ?>
                    <?php
                    $searchResult = json_decode($jsonResponse, true);
                    include 'search_result_display.php';
                    ?>
                <?php endif; ?>
            </div>
        </div>
    </div>
    <script>
        function submitSearch(callsign) {
            var form = document.createElement("form");
            form.method = "POST";
            form.action = "index.php";

            var searchInput = document.createElement("input");
            searchInput.type = "hidden";
            searchInput.name = "search";
            searchInput.value = callsign;
            form.appendChild(searchInput);

            var typeInput = document.createElement("input");
            typeInput.type = "hidden";
            typeInput.name = "type";
            typeInput.value = "call";
            form.appendChild(typeInput);

            document.body.appendChild(form);
            form.submit();
        }
    </script>
</body>

</html>